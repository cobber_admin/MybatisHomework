package site.zhanjingbo.dao;

import site.zhanjingbo.model.Product;
import site.zhanjingbo.model.User;

public interface Op {
	public User getUser(int id);

	public Product getProduct(int id);
}
