package site.zhanjingbo.main;

import org.apache.ibatis.session.SqlSession;

import site.zhanjingbo.dao.Op;
import site.zhanjingbo.model.Product;
import site.zhanjingbo.model.User;
import site.zhanjingbo.tool.SqlSessionTool;

public class Main {
	public static void main(String[] args) {
		SqlSession sqlSession = SqlSessionTool.openSession();
		try {
			Op dao = sqlSession.getMapper(Op.class);
			Product product = dao.getProduct(1);
			System.out.println(product);
			User user = dao.getUser(1);
			System.out.println(user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
	}
}
